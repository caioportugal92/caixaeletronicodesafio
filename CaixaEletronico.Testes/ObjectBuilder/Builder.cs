﻿namespace CaixaEletronico.Testes.ObjectBuilder
{
    public class Builder
    {
        public CaixaBuilder CaixaBuilder = new CaixaBuilder();
        public NotasBuilder NotasBuilder = new NotasBuilder();
        public SaqueBuilder SaqueBuilder = new SaqueBuilder();
    }
}
