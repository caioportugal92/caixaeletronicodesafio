﻿using CaixaEletronico.Modelos;

namespace CaixaEletronico.Testes.ObjectBuilder
{
    public class NotasBuilder
    {
        private int _notasDez = 0;
        private int _notasVinte = 0;
        private int _notasCem = 0;
        private int _notasCinquenta = 0;

        public NotasBuilder ComNotasDez(int notasDez)
        {
            _notasDez = notasDez;
            return this;
        }

        public NotasBuilder ComNotasVinte(int notasVinte)
        {
            _notasVinte = notasVinte;
            return this;
        }

        public NotasBuilder ComNotasCinquenta(int notasCinquenta)
        {
            _notasCinquenta = notasCinquenta;
            return this;
        }

        public NotasBuilder ComNotasCem(int notasCem)
        {
            _notasCem = notasCem;
            return this;
        }

        private Notas Build() =>  new Notas(_notasDez,
                                            _notasVinte,
                                            _notasCem,
                                            _notasCinquenta);
        
        public static implicit operator Notas(NotasBuilder builder) => builder.Build();
    }
}