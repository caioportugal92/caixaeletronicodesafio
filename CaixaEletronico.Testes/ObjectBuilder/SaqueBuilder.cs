﻿using CaixaEletronico.Modelos;
using System;

namespace CaixaEletronico.Testes.ObjectBuilder
{
    public class SaqueBuilder
    {
        private DateTime _horario;
        private int _valor;
        private int _caixaID;

        public SaqueBuilder ComValor(int valor)
        {
            _valor = valor;
            return this;
        }

        public SaqueBuilder ComHorario(DateTime horario)
        {
            _horario = horario;
            return this;
        }

        public SaqueBuilder ComCaixaID(int caixaID)
        {
            _caixaID = caixaID;
            return this;
        }

        private Saque Build()
        {
            return new Saque()
            {
                CaixaID = _caixaID,
                Valor = _valor,
                Horario = _horario
            };
        }

        public static implicit operator Saque(SaqueBuilder builder) => builder.Build();
    }
}