﻿using CaixaEletronico.Modelos;
using System.Collections.Generic;

namespace CaixaEletronico.Testes.ObjectBuilder
{
    public class CaixaBuilder
    {
        private int _identificador;
        private bool _disponivel;
        private Notas _notas;
        private List<Saque> _saques = new List<Saque>();
        public CaixaBuilder ComIdentificador(int identificador)
        {
            _identificador = identificador;
            return this;
        }

        public CaixaBuilder ComCaixaDisponivel(bool disponivel)
        {
            _disponivel = disponivel;
            return this;
        }
        public CaixaBuilder ComNotas(Notas notas)
        {
            _notas = notas;
            return this;
        }
        
        public CaixaBuilder ComSaquesEfetuados(List<Saque> saques)
        {
            _saques = saques;
            return this;
        }

        private Caixa Build()
        {
            return new Caixa()
            {
                Identificador = _identificador,
                CaixaDisponivel = _disponivel,
                Notas = _notas,
                SaquesEfetuados = _saques
            };
        }

        public static implicit operator Caixa(CaixaBuilder caixaBuilder) => caixaBuilder.Build();
    }
}