using CaixaEletronico.Modelos;
using CaixaEletronico.Services;
using CaixaEletronico.TratamentoErro;
using CaixaEletronico.Testes.ObjectBuilder;
using Moq;
using Xunit;
using CaixaEletronico.Dados;

namespace CaixaEletronico.Testes
{
    public class AbastecimentoTestes
    {
        [Fact]
        public void AbastecerCaixaEletronico_RealizarPrimeiroAbastecimento_RetornarSemErro()
        {
            //Arrange
            Notas notasPrimeiroAbastecimento = new Builder()
                                             .NotasBuilder
                                             .ComNotasCem(30)
                                             .ComNotasCinquenta(10)
                                             .ComNotasVinte(50)
                                             .ComNotasDez(100);
            Caixa primeiroAbastecimento = new Builder()
                                          .CaixaBuilder
                                          .ComIdentificador(1)
                                          .ComCaixaDisponivel(false)
                                          .ComNotas(notasPrimeiroAbastecimento);
            var fluxoOperacoesMock = new Mock<IFluxoOperacao>();
            fluxoOperacoesMock.Setup(_ => _.ObterCaixa(It.IsAny<int>())).Returns((Caixa)null);
            var abastecimentoCaixa = new AbastecimentoCaixa(fluxoOperacoesMock.Object);
            //Act
            var saidaOperacao = abastecimentoCaixa.AbastecerCaixaEletronico(primeiroAbastecimento);
            //Assert
            fluxoOperacoesMock.Verify(_ => _.ObterCaixa(It.IsAny<int>()));
            fluxoOperacoesMock.Verify(_ => _.ModificarCaixa(It.IsAny<Caixa>()));
            Assert.Empty(saidaOperacao.Erros);
            Assert.Equal(1, saidaOperacao.Caixa.Identificador);
            Assert.False(saidaOperacao.Caixa.CaixaDisponivel);
            Assert.Equal(100, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(50, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(10, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(30, saidaOperacao.Caixa.Notas.NotasCem);
        }

        [Fact]
        public void AbastecerCaixaEletronico_RealizarDisponibilizacaoCaixaAposPrimeiroAbastecimento_RetornarSemErro()
        {
            // Arrange
            Notas notasPrimeiroAbastecimento = new Builder()
                                             .NotasBuilder
                                             .ComNotasCem(30)
                                             .ComNotasCinquenta(10)
                                             .ComNotasVinte(50)
                                             .ComNotasDez(100);
            Caixa primeiroAbastecimento = new Builder()
                                          .CaixaBuilder
                                          .ComIdentificador(1)
                                          .ComCaixaDisponivel(false)
                                          .ComNotas(notasPrimeiroAbastecimento);
            var fluxoOperacoesMock = new Mock<IFluxoOperacao>();
            fluxoOperacoesMock.Setup(_ => _.ObterCaixa(It.IsAny<int>()))
                              .Returns(primeiroAbastecimento);
            Notas notasSegundoAbastecimento = new Builder()
                                             .NotasBuilder
                                             .ComNotasCem(15)
                                             .ComNotasCinquenta(5)
                                             .ComNotasVinte(5)
                                             .ComNotasDez(5);
            Caixa segundoAbastecimento = new Builder()
                                          .CaixaBuilder
                                          .ComIdentificador(1)
                                          .ComCaixaDisponivel(true)
                                          .ComNotas(notasSegundoAbastecimento);

            var abastecimento = new AbastecimentoCaixa(fluxoOperacoesMock.Object);
            //Act
            var saidaOperacao = abastecimento.AbastecerCaixaEletronico(segundoAbastecimento);
            //Assert
            fluxoOperacoesMock.Verify(_ => _.ObterCaixa(It.IsAny<int>()));
            fluxoOperacoesMock.Verify(_ => _.ModificarCaixa(It.IsAny<Caixa>()));
            Assert.Empty(saidaOperacao.Erros);
            Assert.Equal(1, saidaOperacao.Caixa.Identificador);
            Assert.True(saidaOperacao.Caixa.CaixaDisponivel);
            Assert.Equal(5, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(5, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(5, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(15, saidaOperacao.Caixa.Notas.NotasCem);
        } 
        
        [Fact]
        public void AbastecerCaixaEletronico_TentarAbasterCaixaQueEstaEmUso_RetornarCaixaEmUso()
        {
            //Arrange
            Notas notasPrimeiroAbastecimento = new Builder()
                                             .NotasBuilder
                                             .ComNotasCem(30)
                                             .ComNotasCinquenta(10)
                                             .ComNotasVinte(50)
                                             .ComNotasDez(100);
            Caixa primeiroAbastecimento = new Builder().
                                                CaixaBuilder.
                                                ComCaixaDisponivel(true).
                                                ComIdentificador(1).
                                                ComNotas(notasPrimeiroAbastecimento);
            var fluxoOperacoesMock = new Mock<IFluxoOperacao>();
            fluxoOperacoesMock.Setup(_ => _.ObterCaixa(It.IsAny<int>()))
                              .Returns(primeiroAbastecimento);
            Notas notasSegundoAbastecimento = new Builder()
                                             .NotasBuilder
                                             .ComNotasCem(100)
                                             .ComNotasCinquenta(2500)
                                             .ComNotasVinte(1500)
                                             .ComNotasDez(350);
            Caixa segundoCarregamentoCaixa = new Builder().
                                               CaixaBuilder.
                                               ComCaixaDisponivel(true).
                                               ComIdentificador(1).
                                               ComNotas(notasSegundoAbastecimento);
            var abastecimento = new AbastecimentoCaixa(fluxoOperacoesMock.Object);
            //Act
            var saidaOperacao = abastecimento.AbastecerCaixaEletronico(segundoCarregamentoCaixa);
            //Assert
            fluxoOperacoesMock.Verify(_ => _.ObterCaixa(It.IsAny<int>()));
            Assert.Single(saidaOperacao.Erros);
            Assert.Collection(saidaOperacao.Erros,
                erro => Assert.Equal(MensagemErro.CaixaEmUso, erro));
            Assert.Equal(1, saidaOperacao.Caixa.Identificador);
            Assert.True(saidaOperacao.Caixa.CaixaDisponivel);
            Assert.Equal(100, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(50, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(10, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(30, saidaOperacao.Caixa.Notas.NotasCem);
        }
    }
}
