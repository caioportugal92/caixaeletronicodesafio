﻿using CaixaEletronico.Dados;
using CaixaEletronico.Modelos;
using CaixaEletronico.Services;
using CaixaEletronico.Testes.ObjectBuilder;
using CaixaEletronico.TratamentoErro;
using Moq;
using System;
using Xunit;


namespace CaixaEletronico.Testes
{
    public class SaqueTestes
    {
        [Fact]
        public void RealizarSaque_TendoTodasNotasDisponiveis_RetornarSemErro()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(30)
                         .ComNotasCinquenta(10)
                         .ComNotasVinte(50)
                         .ComNotasDez(100);
            Caixa caixa = new Builder().
                            CaixaBuilder.
                            ComCaixaDisponivel(true).
                            ComIdentificador(1).
                            ComNotas(notas);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.ModificarCaixa(caixa));
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(80)
                       .ComCaixaID(identificadorCaixa);
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ModificarCaixa(caixa));
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            Assert.Empty(saidaOperacao.Erros);
            Assert.Equal(9,saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(49, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(99, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(30, saidaOperacao.Caixa.Notas.NotasCem);
        }

        [Fact]
        public void RealizarSaque_RealizarSaqueValorExatoDeUmaNotaPoremSemNotasDisponiveisDaqueleValor_RetornarSemErro()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(0)
                         .ComNotasCinquenta(10)
                         .ComNotasVinte(20)
                         .ComNotasDez(100);
            Caixa caixa = new Builder().
                            CaixaBuilder.
                            ComCaixaDisponivel(true).
                            ComIdentificador(1).
                            ComNotas(notas);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.ModificarCaixa(caixa));
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(100)
                       .ComCaixaID(identificadorCaixa);

            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ModificarCaixa(caixa));
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            Assert.Empty(saidaOperacao.Erros);
            Assert.Equal(8, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(20, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(100, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCem);
        }
        [Fact]
        public void RealizarSaque_ValorExatoDisponivelNoCaixa_RetornarSemErro()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(0)
                         .ComNotasCinquenta(1)
                         .ComNotasVinte(0)
                         .ComNotasDez(0);
            Caixa caixa = new Builder().
                            CaixaBuilder.
                            ComCaixaDisponivel(true).
                            ComIdentificador(1).
                            ComNotas(notas);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.ModificarCaixa(caixa));
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(50)
                       .ComCaixaID(identificadorCaixa);
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ModificarCaixa(caixa));
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            Assert.Empty(saidaOperacao.Erros);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCem);
        }

        [Fact]
        public void RealizarSaque_ValorIndisponivelParaSaque_RetornarValorIndisponivel()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(0)
                         .ComNotasCinquenta(1)
                         .ComNotasVinte(0)
                         .ComNotasDez(0);
            Caixa caixa = new Builder().
                            CaixaBuilder.
                            ComCaixaDisponivel(true).
                            ComIdentificador(1).
                            ComNotas(notas);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.ModificarCaixa(caixa));
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(500)
                       .ComCaixaID(identificadorCaixa);
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            Assert.Single(saidaOperacao.Erros);
            Assert.Collection(saidaOperacao.Erros,
                              erro => Assert.Equal(MensagemErro.ValorIndisponivel, erro));
            Assert.Equal(1, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCem);
        }


        [Fact]
        public void RealizarSaque_QuandoNaoExistirCaixa_RetornarCaixaInexistente()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 10;
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns((Caixa)null);
            var saque = new Saque()
            {
                CaixaID = identificadorCaixa,
                Horario = DateTime.Now,
                Valor = 0
            };
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            Assert.Single(saidaOperacao.Erros);
            Assert.Collection(saidaOperacao.Erros,
                              erro => Assert.Equal(MensagemErro.CaixaInexistente, erro));
            Assert.Null(saidaOperacao.Caixa);
        }

        [Fact]
        public void RealizarSaque_CaixaExistentePoremIndisponivelParaUso_RetornarCaixaIndisponivel()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(0)
                         .ComNotasCinquenta(1)
                         .ComNotasVinte(0)
                         .ComNotasDez(0);
            Caixa caixa = new Builder().
                            CaixaBuilder.
                            ComCaixaDisponivel(false).
                            ComIdentificador(1).
                            ComNotas(notas);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.ModificarCaixa(caixa));
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(50)
                       .ComCaixaID(identificadorCaixa);
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            Assert.Single(saidaOperacao.Erros);
            Assert.Collection(saidaOperacao.Erros,
                             erro => Assert.Equal(MensagemErro.CaixaIndisponivel, erro));
            Assert.Equal(1, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCem);
        }
        
        [Fact]
        public void RealizarSaque_SaqueDuplicado_RetornarSaqueDuplicado()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(0)
                         .ComNotasCinquenta(1)
                         .ComNotasVinte(0)
                         .ComNotasDez(0);
            Caixa caixa = new Builder().
                            CaixaBuilder.
                            ComCaixaDisponivel(true).
                            ComIdentificador(1).
                            ComNotas(notas);
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(50)
                       .ComCaixaID(identificadorCaixa);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.IsSaqueDuplicado(saque))
                             .Returns(true);
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            fluxoOperacaoMock.Verify(_ => _.IsSaqueDuplicado(saque));
            Assert.Single(saidaOperacao.Erros);
            Assert.Collection(saidaOperacao.Erros,
                             erro => Assert.Equal(MensagemErro.SaqueDuplicado, erro));
            Assert.Equal(1, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCem);
        }

        [Fact] 
        public void RealizarSaque_SemDuplicidade_RetornarSemErro()
        {
            //Arrange
            var fluxoOperacaoMock = new Mock<IFluxoOperacao>();
            var identificadorCaixa = 1;
            Notas notas = new Builder()
                         .NotasBuilder
                         .ComNotasCem(0)
                         .ComNotasCinquenta(1)
                         .ComNotasVinte(0)
                         .ComNotasDez(0);
            Caixa caixa = new Builder()
                        .CaixaBuilder
                        .ComCaixaDisponivel(true)
                        .ComIdentificador(1)
                        .ComNotas(notas);
            Saque saque = new Builder()
                       .SaqueBuilder
                       .ComHorario(DateTime.Now)
                       .ComValor(50)
                       .ComCaixaID(identificadorCaixa);
            fluxoOperacaoMock.Setup(_ => _.ObterCaixa(identificadorCaixa))
                             .Returns(caixa);
            fluxoOperacaoMock.Setup(_ => _.IsSaqueDuplicado(saque))
                             .Returns(false);
            fluxoOperacaoMock.Setup(_ => _.ModificarCaixa(caixa));
            ISaque saqueService = new SaqueService(fluxoOperacaoMock.Object);
            //Act
            var saidaOperacao = saqueService.RealizarSaque(saque);
            //Assert
            fluxoOperacaoMock.Verify(_ => _.ObterCaixa(identificadorCaixa));
            fluxoOperacaoMock.Verify(_ => _.IsSaqueDuplicado(saque));
            fluxoOperacaoMock.Verify(_ => _.ModificarCaixa(caixa));
            Assert.Empty(saidaOperacao.Erros);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCinquenta);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasVinte);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasDez);
            Assert.Equal(0, saidaOperacao.Caixa.Notas.NotasCem);
        }
    }
}
