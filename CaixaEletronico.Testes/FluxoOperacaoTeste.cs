﻿using CaixaEletronico.Dados;
using CaixaEletronico.Modelos;
using CaixaEletronico.Testes.ObjectBuilder;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace CaixaEletronico.Testes
{
    public class FluxoOperacaoTeste
    {       
        [Fact]
        public void IsSaqueDuplicado_SaqueComValorDeterminadoEmPeriodoDezMinutos_RetornarVerdadeiro()
        {
            //Arrage
            var caixas = new List<Caixa>();
            var saquesRealizados = new List<Saque>();
            var dataPrimeiroSaque = new DateTime(2020, 10, 10, 0, 30, 10);
            Saque saqueRealizado = new Builder()
                                  .SaqueBuilder
                                  .ComHorario(dataPrimeiroSaque)
                                  .ComValor(80)
                                  .ComCaixaID(1);
            saquesRealizados.Add(saqueRealizado);
            Notas notasCaixa = new Builder()
                                .NotasBuilder
                                .ComNotasCem(30)
                                .ComNotasCinquenta(10)
                                .ComNotasVinte(50)
                                .ComNotasDez(100);
            Caixa caixa = new Builder()
                            .CaixaBuilder
                            .ComIdentificador(1)
                            .ComCaixaDisponivel(true)
                            .ComSaquesEfetuados(saquesRealizados)
                            .ComNotas(notasCaixa);
            caixas.Add(caixa);
            var dataNovoSaque = new DateTime(2020, 10, 10, 0, 32, 10);
            Saque novoSaque = new Builder()
                         .SaqueBuilder
                         .ComHorario(dataNovoSaque)
                         .ComValor(80)
                         .ComCaixaID(1);
            var mockContexto = new Mock<IContexto>();
            mockContexto.Setup(_ => _.Caixas).Returns(caixas);
            IFluxoOperacao fluxoOperacao = new FluxoOperacao(mockContexto.Object);
            //Act
            var saqueDuplicado = fluxoOperacao.IsSaqueDuplicado(novoSaque);
            //Assert
            Assert.True(saqueDuplicado);
        }

        [Fact]
        public void IsSaqueDuplicado_SaqueComValorDiferenteEmPeriodoDezMinutos_RetornarFalso()
        {
            //Arrage
            var caixas = new List<Caixa>();
            var saquesRealizados = new List<Saque>();
            var dataPrimeiroSaque = new DateTime(2020, 10, 10, 0, 30, 10);
            Saque saqueRealizado = new Builder()
                         .SaqueBuilder
                         .ComHorario(dataPrimeiroSaque)
                         .ComValor(80)
                         .ComCaixaID(1);
            saquesRealizados.Add(saqueRealizado);
            Notas notasCaixa = new Builder()
                                .NotasBuilder
                                .ComNotasCem(30)
                                .ComNotasCinquenta(10)
                                .ComNotasVinte(50)
                                .ComNotasDez(100);
            Caixa caixa = new Builder()
                        .CaixaBuilder
                        .ComIdentificador(1)
                        .ComCaixaDisponivel(true)
                        .ComSaquesEfetuados(saquesRealizados)
                        .ComNotas(notasCaixa);
            caixas.Add(caixa);
            var dataNovoSaque = new DateTime(2020, 10, 10, 0, 32, 10);
            Saque novoSaque = new Builder()
                         .SaqueBuilder
                         .ComHorario(dataNovoSaque)
                         .ComValor(400)
                         .ComCaixaID(1);
            var mockContexto = new Mock<IContexto>();
            mockContexto.Setup(_ => _.Caixas).Returns(caixas);
            IFluxoOperacao fluxoOperacao = new FluxoOperacao(mockContexto.Object);
            //Act
            var saqueDuplicado = fluxoOperacao.IsSaqueDuplicado(novoSaque);
            //Assert
            Assert.False(saqueDuplicado);
        }

        [Fact]
        public void IsSaqueDuplicado_SaqueComValorIgualEmMaiorQueDezMinutos_RetornarFalso()
        {
            //Arrage
            var caixas = new List<Caixa>();
            var saquesRealizados = new List<Saque>();
            var dataPrimeiroSaque = new DateTime(2020, 10, 10, 0, 30, 10);
            Saque saqueRealizado = new Builder()
                         .SaqueBuilder
                         .ComHorario(dataPrimeiroSaque)
                         .ComValor(80)
                         .ComCaixaID(1);
            saquesRealizados.Add(saqueRealizado);
            Notas notasCaixa = new Builder()
                            .NotasBuilder
                            .ComNotasCem(30)
                            .ComNotasCinquenta(10)
                            .ComNotasVinte(50)
                            .ComNotasDez(100);
            Caixa caixa = new Builder()
                        .CaixaBuilder
                        .ComIdentificador(1)
                        .ComCaixaDisponivel(true)
                        .ComSaquesEfetuados(saquesRealizados)
                        .ComNotas(notasCaixa);
            caixas.Add(caixa);
            var dataNovoSaque = new DateTime(2020, 10, 10, 0, 50, 10);
            var novoSaque = new Saque
            {
                CaixaID = 1,
                Horario = dataNovoSaque,
                Valor = 80
            };
            var mockContexto = new Mock<IContexto>();
            mockContexto.Setup(_ => _.Caixas).Returns(caixas);
            IFluxoOperacao fluxoOperacao = new FluxoOperacao(mockContexto.Object);
            //Act
            var saqueDuplicado = fluxoOperacao.IsSaqueDuplicado(novoSaque);
            //Assert
            Assert.False(saqueDuplicado);
        }

        [Fact]
        public void IsSaqueDuplicado_SaqueComDiferenteEmMaiorQueDezMinutos_RetornarFalso()
        {
            //Arrage
            var caixas = new List<Caixa>();
            var saquesRealizados = new List<Saque>();
            var dataPrimeiroSaque = new DateTime(2020, 10, 10, 0, 30, 10);
            Saque saqueRealizado = new Builder()
                                   .SaqueBuilder
                                   .ComHorario(dataPrimeiroSaque)
                                   .ComValor(80)
                                   .ComCaixaID(1);
            saquesRealizados.Add(saqueRealizado);
            Notas notasCaixa = new Builder()
                                .NotasBuilder
                                .ComNotasCem(30)
                                .ComNotasCinquenta(10)
                                .ComNotasVinte(50)
                                .ComNotasDez(100);
            Caixa caixa = new Builder()
                            .CaixaBuilder
                            .ComIdentificador(1)
                            .ComCaixaDisponivel(true)
                            .ComSaquesEfetuados(saquesRealizados)
                            .ComNotas(notasCaixa);
            caixas.Add(caixa);
            var dataNovoSaque = new DateTime(2020, 10, 10, 0, 50, 10);
            Saque novoSaque = new Builder()
                           .SaqueBuilder
                           .ComHorario(dataNovoSaque)
                           .ComValor(400)
                           .ComCaixaID(1);
            var mockContexto = new Mock<IContexto>();
            mockContexto.Setup(_ => _.Caixas).Returns(caixas);
            IFluxoOperacao fluxoOperacao = new FluxoOperacao(mockContexto.Object);
            //Act
            var saqueDuplicado = fluxoOperacao.IsSaqueDuplicado(novoSaque);
            //Assert
            Assert.False(saqueDuplicado);
        }
    }
}
