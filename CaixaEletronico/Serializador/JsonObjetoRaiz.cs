﻿using CaixaEletronico.Modelos;
using Newtonsoft.Json;

namespace CaixaEletronico.Serializador
{
    public class JsonObjetoRaiz
    {
        [JsonProperty("saque")]
        public Saque Saque { get; set; }
        [JsonProperty("caixa")]
        public Caixa Caixa { get; set; }
    }
}
