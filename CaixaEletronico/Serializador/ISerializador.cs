﻿using CaixaEletronico.Modelos;

namespace CaixaEletronico.Serializador
{
    public interface ISerializador
    {
        Caixa ObterCaixa(string json);
        Saque ObterSaque(string json);
    }
}
