﻿using CaixaEletronico.Modelos;
using Newtonsoft.Json;

namespace CaixaEletronico.Serializador
{
    public class SerializadorOperacao : ISerializador
    {
        public Caixa ObterCaixa(string json)
        {
            var raizJsonSaque = JsonConvert.DeserializeObject<JsonObjetoRaiz>(json);
            return raizJsonSaque.Caixa;
        }
        public Saque ObterSaque(string json){
            var raizJsonSaque = JsonConvert.DeserializeObject<JsonObjetoRaiz>(json);
            return raizJsonSaque.Saque;
        }

    }
}
