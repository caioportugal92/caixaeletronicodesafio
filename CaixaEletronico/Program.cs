﻿using Microsoft.Extensions.DependencyInjection;
using System;
using CaixaEletronico.Dados;
using CaixaEletronico.Services;
using Microsoft.Extensions.Hosting;
using CaixaEletronico.Serializador;

namespace CaixaEletronico
{
    class Program
    {
        static void Main(string[] args)
        {
            var hostBuilder = CreateHostBuilder(args).Build();
            var operacao = hostBuilder.Services.GetService<IOperacao>();
            Console.WriteLine("**** Para finalizar digite Exit  ****");
            string input = string.Empty;
            while ((input = Console.ReadLine()) != null)
            {
                if (FinalizarProcessamento(input))
                    Environment.Exit(0);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(operacao.ProcessarOperacao(input).ToString());
                Console.ResetColor();
            }
        }
        private static bool FinalizarProcessamento(string input) => "Exit".Equals(input, StringComparison.InvariantCultureIgnoreCase);

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host
                  .CreateDefaultBuilder(args)
                  .ConfigureServices((hostContext, services) => DefinirInjecoesDependencias(services));
        }
        private static void DefinirInjecoesDependencias(IServiceCollection services)
        {
            services.AddScoped<IOperacao, Operacao>();
            services.AddScoped<ISerializador, SerializadorOperacao>();
            services.AddScoped<IFluxoOperacao, FluxoOperacao>();
            services.AddScoped<IAbastecimento, AbastecimentoCaixa>();
            services.AddScoped<ISaque, SaqueService>();
            services.AddSingleton<IContexto, Contexto>();
        }
    }
}
