﻿using CaixaEletronico.Modelos;

namespace CaixaEletronico.Services
{
    public interface IAbastecimento
    {
        SaidaOperacao AbastecerCaixaEletronico(Caixa caixa);
    }
}
