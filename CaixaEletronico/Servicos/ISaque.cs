﻿using CaixaEletronico.Modelos;

namespace CaixaEletronico.Services
{
    public interface ISaque
    {
        SaidaOperacao RealizarSaque(Saque saque);
    }
}
