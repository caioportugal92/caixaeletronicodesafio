﻿using CaixaEletronico.Modelos;
using CaixaEletronico.Serializador;
using System;

namespace CaixaEletronico.Services
{
    public class Operacao : IOperacao
    {
        private ISaque _saque;
        private IAbastecimento _abastecimento;
        private ISerializador _serializador;

        public Operacao(ISerializador serializador, ISaque saqueService, IAbastecimento abastecimentoService)
        {
            _saque = saqueService;
            _abastecimento = abastecimentoService;
            _serializador = serializador;
        }

        public SaidaOperacao ProcessarOperacao(string json)
        {
            if (json.Contains("caixaID", StringComparison.InvariantCultureIgnoreCase))
                return _saque.RealizarSaque(_serializador.ObterSaque(json));
            return _abastecimento.AbastecerCaixaEletronico(_serializador.ObterCaixa(json));
        }
    }
}
