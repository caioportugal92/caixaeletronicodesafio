﻿using CaixaEletronico.Modelos;

namespace CaixaEletronico.Services
{
    public interface IOperacao
    {
        SaidaOperacao ProcessarOperacao(string json);
    }
}
