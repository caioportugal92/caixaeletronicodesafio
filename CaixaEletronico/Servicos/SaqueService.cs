﻿using CaixaEletronico.Dados;
using CaixaEletronico.Modelos;
using CaixaEletronico.TratamentoErro;

namespace CaixaEletronico.Services
{
    public class SaqueService : ISaque
    {
        private IFluxoOperacao _fluxoOperacao;
        public SaqueService(IFluxoOperacao fluxoOperacao)
        {
            _fluxoOperacao = fluxoOperacao;
        }

        public SaidaOperacao RealizarSaque(Saque saque)
        {
            var caixa = _fluxoOperacao.ObterCaixa(saque.CaixaID);
            if (!ValidarSaque(caixa, saque, out SaidaOperacao saidaOperacao))
            {
                saidaOperacao.Caixa = caixa;
                return saidaOperacao;
            }
            caixa.RealizarSaque(saque);
            _fluxoOperacao.ModificarCaixa(caixa);
            saidaOperacao.Caixa = caixa;
            return saidaOperacao;
        }

        private bool ValidarSaque(Caixa caixa, Saque saque, out SaidaOperacao saidaOperacao)
        {
            saidaOperacao = new SaidaOperacao();
            if(caixa == null)
            {
                saidaOperacao.RegistrarErro(MensagemErro.CaixaInexistente);
                return false;
            }
            if (!caixa.IsCaixaDisponivel())
            {
                saidaOperacao.RegistrarErro(MensagemErro.CaixaIndisponivel);
                return false;
            }
            if (!caixa.IsValorSaqueDisponivel(saque.Valor))
            {
                saidaOperacao.RegistrarErro(MensagemErro.ValorIndisponivel);
                return false;
            }
            if (_fluxoOperacao.IsSaqueDuplicado(saque))
            {
                saidaOperacao.RegistrarErro(MensagemErro.SaqueDuplicado);
                return false;
            }
            return true;
        }
    }
}
