﻿using CaixaEletronico.Dados;
using CaixaEletronico.Modelos;
using CaixaEletronico.TratamentoErro;

namespace CaixaEletronico.Services
{
    public class AbastecimentoCaixa : IAbastecimento
    {
        private IFluxoOperacao _fluxoOperacoes;
        public AbastecimentoCaixa(IFluxoOperacao fluxoOperacoes)
        {
            _fluxoOperacoes = fluxoOperacoes;
        }
        public SaidaOperacao AbastecerCaixaEletronico(Caixa caixaAbastecimento)
        {
            var caixaPreExistente = _fluxoOperacoes.ObterCaixa(caixaAbastecimento.Identificador);
            if(!ValidarAbastecimento(caixaAbastecimento, caixaPreExistente, out SaidaOperacao saidaOperacao))
            {
                saidaOperacao.Caixa = caixaPreExistente;
                return saidaOperacao;
            }
            _fluxoOperacoes.ModificarCaixa(caixaAbastecimento);
            saidaOperacao.Caixa = caixaAbastecimento;
            return saidaOperacao;
        }

        public bool ValidarAbastecimento(Caixa caixa,Caixa caixaPreExistente, out SaidaOperacao saidaOperacao)
        {
            saidaOperacao = new SaidaOperacao();
            if (caixaPreExistente != null && caixaPreExistente.CaixaDisponivel)
            {
                saidaOperacao.RegistrarErro(MensagemErro.CaixaEmUso);                
                return false;
            }
            return true;
        }
    }
}
