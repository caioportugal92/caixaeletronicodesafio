﻿namespace CaixaEletronico.TratamentoErro
{
    public class MensagemErro
    {
        public static readonly string CaixaEmUso = "caixa-em-uso";
        public static readonly string ValorIndisponivel = "valor-indisponivel";
        public static readonly string CaixaInexistente = "caixa-inexistente";
        public static readonly string CaixaIndisponivel = "caixa-indisponivel";
        public static readonly string SaqueDuplicado = "saque-duplicado";
    }
}
