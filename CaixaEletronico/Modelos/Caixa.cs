﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CaixaEletronico.Modelos
{
    public class Caixa
    {
        [JsonProperty("identificador")]
        public int Identificador { get; set; }

        [JsonProperty("caixaDisponivel")]
        public bool CaixaDisponivel { get; set; }

        [JsonProperty("notas")]
        public Notas Notas { get; set; }
        public List<Saque> SaquesEfetuados { get; set; }
        public Caixa()
        {
            SaquesEfetuados = new List<Saque>();
        }
        public void RealizarSaque(Saque saque)
        {
            Notas.EntregarNotas(saque.Valor);
            SaquesEfetuados.Add(saque);
        }
        public bool IsValorSaqueDisponivel(int valorSaque) => valorSaque <= Notas.CalcularValorEmNotasDisponiveis();
        public bool IsCaixaDisponivel() => this != null && CaixaDisponivel;
    }                          
}
