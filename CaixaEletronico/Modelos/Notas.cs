﻿using Newtonsoft.Json;

namespace CaixaEletronico.Modelos
{
    public class Notas
    {
        private readonly int ValorNotaCem = 100;
        private readonly int ValorNotaCinquenta = 50;
        private readonly int ValorNotaVinte = 20;
        private readonly int ValorNotaDez = 10;
        [JsonProperty("notasDez")]
        public int NotasDez { get; set; }

        [JsonProperty("notasVinte")]
        public int NotasVinte { get; set; }

        [JsonProperty("notasCinquenta")]
        public int NotasCinquenta { get; set; }

        [JsonProperty("notasCem")]
        public int NotasCem { get; set; }

        public Notas(int notasDez,
                    int notasVinte,
                    int notasCem,
                    int notasCinquenta)
        {
            NotasDez = notasDez;
            NotasVinte = notasVinte;
            NotasCem = notasCem;
            NotasCinquenta = notasCinquenta;
        }

        public void EntregarNotas(int valorSaque)
        {
            var valorRestanteParaSaque = EntregarNotasCem(valorSaque);
            valorRestanteParaSaque = EntregarNotasCinquenta(valorRestanteParaSaque);
            valorRestanteParaSaque = EntregarNotasVinte(valorRestanteParaSaque);
            EntregarNotasDez(valorRestanteParaSaque);
        }

        public int CalcularValorEmNotasDisponiveis()
        {
            return (ValorNotaCem * NotasCem) +
                   (ValorNotaCinquenta * NotasCinquenta) +
                   (ValorNotaVinte * NotasVinte) +
                   (ValorNotaDez * NotasDez);
        }

        private int EntregarNotasDez(int valorSaque)
        {
            var notasSacadas = EntregarNotas(NotasDez, ValorNotaDez, valorSaque);
            NotasDez = NotasDez - notasSacadas;
            return CalcularValorRestanteSaque(notasSacadas, ValorNotaDez, valorSaque);
        }

        private int EntregarNotasVinte(int valorSaque)
        {
            var notasSacadas = EntregarNotas(NotasVinte, ValorNotaVinte, valorSaque);
            NotasVinte = NotasVinte - notasSacadas;
            return CalcularValorRestanteSaque(notasSacadas, ValorNotaVinte, valorSaque);
        }

        private int EntregarNotasCinquenta(int valorSaque)
        {
            var notasSacadas = EntregarNotas(NotasCinquenta, ValorNotaCinquenta, valorSaque);
            NotasCinquenta = NotasCinquenta - notasSacadas;
            return CalcularValorRestanteSaque(notasSacadas, ValorNotaCinquenta, valorSaque);
        }

        private int EntregarNotasCem(int valorSaque)
        {
            var notasSacadas = EntregarNotas(NotasCem, ValorNotaCem, valorSaque);
            NotasCem = NotasCem - notasSacadas;
            return CalcularValorRestanteSaque(notasSacadas, ValorNotaCem, valorSaque);
        }

        private int CalcularValorRestanteSaque(int notasSacadas,int valorNota,int valorSaque) 
            => valorSaque - (notasSacadas * valorNota);
        private int EntregarNotas(int quantidadeNotasDisponiveis, int valorNota, int valorSaque)
        {
            var notasEntregues = 0;
            if (valorSaque < valorNota)
                return notasEntregues;
            var numeroMaximoNotasPossiveis = (valorSaque / valorNota);
            notasEntregues = (numeroMaximoNotasPossiveis >= quantidadeNotasDisponiveis) ?
                             quantidadeNotasDisponiveis :
                             numeroMaximoNotasPossiveis;
            return notasEntregues;
        }
    }
}