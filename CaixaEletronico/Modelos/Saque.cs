﻿using Newtonsoft.Json;
using System;

namespace CaixaEletronico.Modelos
{
    public class Saque
    {
        [JsonProperty("caixaID")]
        public int CaixaID { get; set; }
        [JsonProperty("valor")]
        public int Valor { get; set; }
        [JsonProperty("horario")]
        public DateTime Horario { get; set; }
    }   
}
