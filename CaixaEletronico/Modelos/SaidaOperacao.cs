﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace CaixaEletronico.Modelos
{
    public class SaidaOperacao
    {
        public Caixa Caixa { get; set; }
        public List<string> Erros { get; private set; }
        public SaidaOperacao()
        {
            Erros = new List<string>();
        }
        public void RegistrarErro(string erro) => Erros.Add(erro);

        public override string ToString() => JsonConvert.SerializeObject(this, 
                                              new JsonSerializerSettings 
                                              {
                                                  NullValueHandling = NullValueHandling.Ignore,
                                                  ContractResolver = new DefaultContractResolver
                                                  {
                                                      NamingStrategy = new CamelCaseNamingStrategy()
                                                  }
                                              });
    }
}