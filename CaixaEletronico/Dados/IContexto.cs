﻿using CaixaEletronico.Modelos;
using System.Collections.Generic;

namespace CaixaEletronico.Dados
{
    public interface IContexto
    {
        public List<Caixa> Caixas { get; set; }
    }
}
