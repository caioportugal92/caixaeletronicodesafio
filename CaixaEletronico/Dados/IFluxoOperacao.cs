﻿using CaixaEletronico.Modelos;

namespace CaixaEletronico.Dados
{
    public interface IFluxoOperacao
    {        
        Caixa ObterCaixa(int identificadorCaixa);
        void ModificarCaixa(Caixa caixa);
        bool IsSaqueDuplicado(Saque novoSaque);
    }
}