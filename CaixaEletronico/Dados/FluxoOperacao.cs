﻿using CaixaEletronico.Modelos;
using System.Linq;

namespace CaixaEletronico.Dados
{
    public class FluxoOperacao : IFluxoOperacao
    {
        private readonly int ItemNaoEncontrado = -1;
        private readonly int IntervaloSaqueEmMinutos = 10;
        private IContexto _contexto;
        public FluxoOperacao(IContexto contexto) => _contexto = contexto;

        public bool IsSaqueDuplicado(Saque novoSaque)
        {
            return _contexto
                  .Caixas
                  .Any(_ => _.SaquesEfetuados
                        .Any(_ => _.Valor == novoSaque.Valor &&
                                   (
                                        _.Horario >= novoSaque.Horario.AddMinutes(-IntervaloSaqueEmMinutos)
                                        && 
                                        _.Horario <= novoSaque.Horario
                                    )
                               )
                        );
        }

        public void ModificarCaixa(Caixa caixa)
        {
            var indexCaixa = _contexto.Caixas
                            .FindIndex(x => x.Identificador == caixa.Identificador);
            if (indexCaixa == ItemNaoEncontrado)
            {
                _contexto.Caixas.Add(caixa);
                return;
            }
            _contexto.Caixas[indexCaixa] = caixa;
        }

        public Caixa ObterCaixa(int identificadorCaixa) => _contexto.Caixas
                  .FirstOrDefault(_=>_.Identificador == identificadorCaixa);
    }
}
