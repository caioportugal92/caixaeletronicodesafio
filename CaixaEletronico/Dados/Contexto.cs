﻿using CaixaEletronico.Modelos;
using System.Collections.Generic;

namespace CaixaEletronico.Dados
{
    public class Contexto : IContexto
    {
        public List<Caixa> Caixas { get; set; }

        public Contexto() => Caixas = new List<Caixa>();
    }
}
